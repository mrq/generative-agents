import os
import json
import pickle
import re
import logging

from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple

from pydantic import BaseModel, Field

from langchain import LLMChain
from langchain.base_language import BaseLanguageModel
from langchain.prompts import PromptTemplate
from langchain.schema import BaseMemory, Document
from langchain.retrievers import TimeWeightedVectorStoreRetriever

from langchain.embeddings.base import Embeddings
from langchain.vectorstores.base import VectorStore
from langchain.memory.vectorstore import VectorStoreRetrieverMemory


logger = logging.getLogger(__name__)

from prompts import get_prompt, get_stop_tokens

class GenerativeAgent(BaseModel):
	name: str
	sex: str
	age: Optional[int] = None
	traits: str = "N/A"
	status: str
	memories: List[dict] = Field(default_factory=list)
	summaries: List[str] = Field(default_factory=list)

	last_refreshed: datetime = Field(default_factory=datetime.now)

	llm: Optional[BaseLanguageModel] = None
	embeddings: Optional[Embeddings] = None
	vectorstore: Optional[VectorStore] = None
	memory: Optional[VectorStoreRetrieverMemory] = None

	verbose: bool = True

	class Config:
		arbitrary_types_allowed = True

	@classmethod
	def create(cls, name: str, age: int, sex: str, traits: str, status: str, summaries: List[str] = ["N/A"], memories: List[dict] = [], llm: Optional[BaseLanguageModel] = None, embeddings: Optional[Embeddings] = None, vectorstore: Optional[VectorStore] = None ):
		agent = cls(
			name = name,
			age = age,
			sex = sex,
			traits = traits,
			status = status,
			memories = memories,
			summaries = summaries,
			llm = llm,
			embeddings = embeddings,
			vectorstore = vectorstore,
			memory = VectorStoreRetrieverMemory(
				retriever = vectorstore.as_retriever(),
			)
		)


		if len(agent.memories) > 0:
			agent.vectorstore.add_texts(
				texts=[ memory["observation"] for memory in agent.memories ],
				metadatas=[ { "name": agent.name, "time": memory["time"], "importance": memory["importance"] } for memory in agent.memories ],
			)

		return agent

	def chain(self, prompt: PromptTemplate) -> LLMChain:
		return LLMChain(llm=self.llm, prompt=prompt, verbose=self.verbose)

	def save(self, pickled: bool = False) -> str:
		os.makedirs(f"./agents/", exist_ok=True)
		obj = {
			"name": self.name,
			"age": self.age,
			"sex": self.sex,
			"traits": self.traits,
			"status": self.status,
			"summaries": self.summaries,
			"memories": self.memories,
		}

		if pickled:
			path = f"./agents/{self.name}.pth"
			pickle.dump(obj, open(path, 'wb'))
		else:
			path = f"./agents/{self.name}.json"
			json.dump(obj, open(path, "w", encoding="utf-8"))

	@classmethod
	def load(cls, name: str, llm: Optional[BaseLanguageModel] = None, embeddings: Optional[Embeddings] = None, vectorstore: Optional[VectorStore] = None, pickled: bool = False) -> str:
		if pickled:
			path = f"./agents/{name}.pth"
			obj = pickle.load(open(path, 'rb'))
		else:
			path = f"./agents/{name}.json"
			obj = json.load(open(path, 'r', encoding="utf-8"))

		agent = cls.create(**obj, llm=llm, embeddings=embeddings, vectorstore=vectorstore)

		return agent

	def importance( self, observation: str, weight: float = 0.15 ) -> float:
		prompt = PromptTemplate.from_template(get_prompt("memory_importance"))
		score = self.chain(prompt).run(
			stop=get_stop_tokens(tokens=[".", "/", "("]),
			observation=observation,
		).strip()
		match = re.search(r"(\d+)", score)
		if match:
			score = float(match.group(0))
		else:
			score = 2.0

		return score / 10.0 * weight

	def summarize( self ) -> str:
		prompt = PromptTemplate.from_template(get_prompt('compute_agent_summary'))
		summary = self.chain(prompt).run(
			stop=get_stop_tokens(),
			name=self.name,
			summary=self.summary(),
			memories="\n".join(self.recent_memories())
		).strip()
		self.summaries.append(f'{self.name} {summary}')
		return f'{self.name} {summary}'

	def summary( self, refresh: bool = False ) -> str:
		# todo: invoke summarizer
		if refresh:
			self.summarize()
		return self.summaries[-1]

	def relevant_memories( self, observation: str, k = 12 ) -> List[str]:
		# todo: query vectorstore
		return [ memory["observation"] for memory in self.memories[-k:] ]

	def recent_memories( self, k = 12 ) -> List[str]:
		# todo: sort by time
		return [ memory["observation"] for memory in self.memories[-k:] ]

	def memorize( self, observation: str, importance: float = 0, time: datetime = datetime.now() ) -> dict:
		entry = {
			"time": int(time.timestamp()),
			"importance": importance,
			"observation": observation,
		}
		self.memories.append(entry)
		self.vectorstore.add_texts(
			texts=[ observation ],
			metadatas=[ { "name": self.name, "time": entry["time"], "importance": entry["importance"] } ],
		)
		return entry

	def observe( self, observation: str, importance: float = 0, time: datetime = datetime.now() ) -> float:
		if importance == 0:
			importance = self.importance( observation )
		self.memorize( observation, importance, time )
		return importance

	def react( self, observation: str, history: List[str] = [], time: datetime = datetime.now() ) -> dict:
		# self.memorize( observation )
		suffix = get_prompt('suffix_generate_response')
		prompt = PromptTemplate.from_template(
			get_prompt('generate_reaction').replace("{suffix}", suffix)
		)
		summary = self.summary()
		relevant_memories = self.relevant_memories(observation)
		recent_memories = self.recent_memories()

		# avoid repeating
		memory = ""

		for mem in relevant_memories:
			if mem in summary or mem in memory or mem in observation or mem in history:
				continue
			memory += f"\n{mem}"
		
		for mem in recent_memories:
			if mem in summary or mem in observation or mem in history:
				continue
			# erase it, move it to bottom
			if mem in memory:
				memory = memory.replace(f'{mem}\n', "")
			memory += f"\n{mem}"

		history = "\n".join(history)
		reaction = self.chain(prompt=prompt).run(
			stop=get_stop_tokens(tokens=[f'\n{self.name}: ']),
			current_time=datetime.now().strftime("%B %d, %Y, %I:%M %p"),
			name=self.name,
			status=self.status if self.status else "N/A",
			summary=summary if summary else "N/A",
			memory=memory if memory else "N/A",
			history=history if history else "N/A",
			observation=observation if observation else "N/A",
		).strip()

		emoji_pattern = re.compile("["
				u"\U0001F600-\U0001F64F"  # emoticons
				u"\U0001F300-\U0001F5FF"  # symbols & pictographs
				u"\U0001F680-\U0001F6FF"  # transport & map symbols
				u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
								   "]+", flags=re.UNICODE)
		reaction = emoji_pattern.sub(r'', reaction)

		# cleanup
		reactions = reaction.replace(u"\u200B", "").strip().split("\n")

		for reaction in reactions:
			if reaction in summary or reaction in memory or reaction in history:
				continue
			if reaction:
				break

		return f'{self.name}: {reaction}'