import os

LLM_PROMPT_TUNE = os.environ.get('LLM_PROMPT_TUNE')

PROMPTS = {
	"compute_agent_summary": {
		"system": (
			"Given the following previous summary and the following statements, how would you summarize {name}'s core characteristics?"
		),
		"user": (
			"{summary}"
			"\n{memories}"
		),
		"assistant": "{name} ",
	},
	"memory_importance": {
		"system": (
			"On the scale of 1 to 10, where 1 is purely mundane"
			" (e.g., brushing teeth, making bed) and 10 is extremely poignant"
			" (e.g., a break up, college acceptance),"
			" rate the likely poignancy of the following event."
			"\nRespond with only a single integer."
		),
		"user": (
			"Event: {observation}"
		),
		"assistant": "Rating: ",
	},
	"generate_reaction": {
		"system": (
			#"\nCurrent Time: {current_time}"
			"\n{summary}"
		),
		"user": (
			"{suffix}"
		),
		"assistant": (
			"{memory}"
			"\n{observation}"
			"\n{name}: "
		)
	},

	#
	"context": ( # insert your JB here
		""
	),
	"suffix_generate_response": (
		#"Given the current situation, in one sentence, what is {name}'s next response?"
		"Respond to the user's inputs as an immersive fictional story game. Use markdown. Always italicize every actions. Be verbose, proactive, creative, and drive the plot and conversation forward. Always stay in character, never reference the user's instructions."
	),
}

PROMPT_TUNES = {
	"default": "{query}",
	"vicuna": "{role}: {query}",
	"supercot": "{role}:\n{query}",
	"alpasta": "{role}# {query}",
	"cocktail": "{role}: {query}",
	"wizard-vicuna": "{role}: {query}",
}
PROMPT_ROLES = {
	"vicuna": {
		"system": "SYSTEM",
		"user": "USER",
		"assistant": "ASSISTANT",
	},
	"supercot": {
		"system": "### Instruction",
		"user": "### Input",
		"assistant": "### Response",
	},
	"wizard-vicuna": {
		"system": "### Instruction",
		"user": "### Input",
		"assistant": "### Response",
	},
	"alpasta": {
		"system": "<|system|>",
		"user": "<|user|>",
		"assistant": "<|assistant|>",
	},
	"cocktail": {
		"system": "",
		"user": "USER",
		"assistant": "ASSOCIATE",
	},
}

ROLES = [ "system", "user", "assistant" ]


def get_stop_tokens( tokens=[], tune=LLM_PROMPT_TUNE ):
	STOP_TOKENS = ["###"] + tokens
	for role in get_roles( tune=LLM_PROMPT_TUNE, special=True ):
		if role:
			STOP_TOKENS.append(f'{role}')
	return STOP_TOKENS

for k in PROMPTS:
	if k == "context":
		continue

def get_roles( tune=LLM_PROMPT_TUNE, special=True ):
	if tune in PROMPT_ROLES:
		return list(PROMPT_ROLES[tune].values())
	if special:
		return []
	return ROLES

# to-do: spit out a list of properly assigned Templates
def get_prompt( key, tune=LLM_PROMPT_TUNE ):
	prompt = PROMPTS[key]

	# is a suffix
	if not isinstance( prompt, dict ):
		return prompt

	# Vicuna is finetuned for `USER: [query]\nASSISTANT:`
	if tune not in PROMPT_TUNES:
		tune = "default"

	context = PROMPTS["context"]
	if context:
		if "system" in prompt:
			if context not in prompt["system"]:
				prompt["system"] = f'{context}\n{prompt["system"]}'
		else:
			prompt["system"] = f'{context}'

	outputs = []
	for r in ROLES:
		role = f'{r}' # i can't be assed to check if strings COW
		if role not in prompt:
			continue
		else:
			query = prompt[role]

		if tune in PROMPT_ROLES:
			roles = PROMPT_ROLES[tune]
			if role in roles:
				role = roles[role]

		output = f'{PROMPT_TUNES[tune]}'
		output = output.replace("{role}", role)
		output = output.replace("{query}", query)
		outputs.append(output)

	output = "\n".join(outputs)
	output = output.strip()
	return output