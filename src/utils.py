import logging
logging.basicConfig(level=logging.ERROR)

from datetime import datetime, timedelta
from typing import Any, Callable, Dict, List, Mapping, Optional, Tuple
from termcolor import colored

import os
import copy
import math
import faiss
import re
import pickle
import json
import random

from langchain.schema import Document
from langchain.vectorstores import FAISS
from langchain.docstore import InMemoryDocstore
from langchain.retrievers import TimeWeightedVectorStoreRetriever

from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
callback_manager = CallbackManager([StreamingStdOutCallbackHandler()]) # unncessesary but whatever

# shit I can shove behind an env var
LLM_TYPE = os.environ.get('LLM_TYPE', "llamacpp") # options: llamacpp, oai
LLM_LOCAL_MODEL = os.environ.get('LLM_MODEL', 
	#"./models/ggml-vicuna-13b-1.1/ggml-vic13b-uncensored-q4_2.bin"
	#"./models/ggml-vicuna-13b-cocktail-v1-q5_0.bin"
	#"./models/WizardML-Unc-13b-Q5_1.bin"
	#"./models/llama-13b-supercot-ggml/ggml-model-q4_2.bin"
	#"./models/llama-33b-supercot-ggml/ggml-model-q4_2.bin"
	#"./models/gpt4-x-alpasta-30b-ggml-q4_1.bin"

	#"./models/Wizard-Vicuna-13B-Uncensored.ggml.q5_1.bin"
	"./models/wizardlm-13b-uncensored-ggml-q5_1.bin"
)
LLM_CONTEXT = int(os.environ.get('LLM_CONTEXT', '2048'))
LLM_THREADS = int(os.environ.get('LLM_THREADS', '6'))
LLM_GPU_LAYERS = int(os.environ.get('LLM_GPU_LAYERS', '99'))
LLM_TEMPERATURE = float(os.environ.get('LLM_TEMPERATURE', '0.99'))
EMBEDDING_TYPE = os.environ.get("LLM_EMBEDDING_TYPE", "hf") # options: llamacpp, oai, hf
VECTORSTORE_TYPE = os.environ.get("LLM_VECTORSTORE_TYPE", "chroma") # options: chroma

# deduce a default given a model path
if LLM_TYPE=="oai":
	LLM_PROMPT_TUNE_DEFAULT = "oai"
else:
	if "supercot" in LLM_LOCAL_MODEL.lower():
		LLM_PROMPT_TUNE_DEFAULT = "supercot"
	if "wizard-vicuna" in LLM_LOCAL_MODEL.lower():
		LLM_PROMPT_TUNE_DEFAULT = "wizard-vicuna"
	elif "vicuna" in LLM_LOCAL_MODEL.lower():
		LLM_PROMPT_TUNE_DEFAULT = "vicuna"
	elif "wizard" in LLM_LOCAL_MODEL.lower():
		LLM_PROMPT_TUNE_DEFAULT = "vicuna"
	elif "alpasta" in LLM_LOCAL_MODEL.lower():
		LLM_PROMPT_TUNE_DEFAULT = "alpasta"
	elif "cocktail" in LLM_LOCAL_MODEL.lower():
		LLM_PROMPT_TUNE_DEFAULT = "cocktail"
	else:
		LLM_PROMPT_TUNE_DEFAULT = "llama"

LLM_PROMPT_TUNE = os.environ.get('LLM_PROMPT_TUNE', LLM_PROMPT_TUNE_DEFAULT)
os.environ['LLM_PROMPT_TUNE'] = LLM_PROMPT_TUNE # sync it back to prompts

if LLM_TYPE=="llamacpp":
	from langchain.llms import LlamaCpp

	LLM = LlamaCpp(
		model_path=LLM_LOCAL_MODEL,
		callback_manager=callback_manager,
		verbose=True,
		n_ctx=LLM_CONTEXT,
		n_gpu_layers=LLM_GPU_LAYERS,
		temperature=LLM_TEMPERATURE,
		#n_threads=LLM_THREADS,
		#use_mlock=True,
		#use_mmap=True,
	)
elif LLM_TYPE=="oai":
	from langchain.chat_models import ChatOpenAI

	# Override for Todd
	if os.environ.get('LANGCHAIN_OVERRIDE_RESULT', '1') == '1':
		from langchain.schema import Generation, ChatResult, LLMResult, ChatGeneration
		from langchain.chat_models.openai import _convert_dict_to_message

		def _create_chat_result(self, response: Mapping[str, Any]) -> ChatResult:
			token_usage = { "prompt_tokens": 5, "completion_tokens": 5, "total_tokens": 10 }
			generations = []
			for res in response["choices"]:
				message = _convert_dict_to_message(res["message"])
				gen = ChatGeneration(message=message)
				generations.append(gen)
			llm_output = {"token_usage": response["usage"] if "usage" in response else token_usage, "model_name": self.model_name}
			return ChatResult(generations=generations, llm_output=llm_output)
		ChatOpenAI._create_chat_result = _create_chat_result

	LLM = ChatOpenAI(
		max_tokens=LLM_CONTEXT,
		temperature=LLM_TEMPERATURE,
		model_name=os.environ.get('OPENAI_MODEL_NAME', 'gpt-4'),
	)

else:
	raise f"Invalid LLM type: {LLM_TYPE}"

if EMBEDDING_TYPE == "hf":
	from langchain.embeddings import HuggingFaceEmbeddings

	EMBEDDINGS_MODEL = HuggingFaceEmbeddings()
	EMBEDDINGS_SIZE = 768
elif EMBEDDING_TYPE == "oai":
	from langchain.embeddings import OpenAIEmbeddings

	EMBEDDINGS_MODEL = OpenAIEmbeddings() 
	EMBEDDINGS_SIZE = 1536
elif EMBEDDING_TYPE == "llamacpp":
	from langchain.embeddings import LlamaCppEmbeddings

	EMBEDDINGS_MODEL = LlamaCppEmbeddings(
		model_path=LLM_LOCAL_MODEL,
	)
	EMBEDDINGS_SIZE = 5120
else:
	raise f"Invalid embedding type: {EMBEDDING_TYPE}"

if VECTORSTORE_TYPE == "chroma":
	from langchain.vectorstores import Chroma

	VECTORSTORE = Chroma(embedding_function=EMBEDDINGS_MODEL)
else:
	raise f"Invalid embedding type: {EMBEDDING_TYPE}"

from agent import GenerativeAgent

def create_agent(**kwargs):
	settings = {
		"llm": LLM,
		"embeddings": EMBEDDINGS_MODEL,
		"vectorstore": VECTORSTORE,
	}
	settings.update(kwargs)
	if "summary" in settings:
		if "summaries" not in settings:
			settings["summaries"] = [ settings["summary"] ]
		del settings["summary"]
	return GenerativeAgent.create(**settings)

def save_agent( agent, pickled=False ):
	agent.save()

def load_agent( name, pickled=False ):
	return GenerativeAgent.load(name, llm=LLM, embeddings=EMBEDDINGS_MODEL, vectorstore=VECTORSTORE)

def agent_summary(agent: GenerativeAgent, force_refresh: bool = True) -> str:
	return agent.summary(refresh=force_refresh)

def agent_observes( agent: GenerativeAgent, observations: List[str] ):
	results = []
	for observation in observations:
		observation = observation.replace("{name}", agent.name)
		results.append(agent.observe( observation ))
	return results

def agent_reacts( agent: GenerativeAgent, observations: List[str], record: bool = True ):
	results = []
	for observation in observations:
		observation = observation.replace("{name}", agent.name)
		if record:
			agent.observe( observation )
		results.append(agent.react( observation ))
	return results

def agent_interview(agent: GenerativeAgent, message: str, record: bool = False ) -> str:
	message = message.replace("{name}", agent.name)
	if record:
		agent.observe( message )
	return agent.react( message )